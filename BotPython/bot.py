import os

import discord
from discord.ext import commands
from dotenv import load_dotenv
import requests

load_dotenv(dotenv_path=".env")

token = os.getenv("TOKEN")
api = os.getenv("API")

headers = {'Content-type' : 'application/json', 'Accept' : 'application/json'}

default_intents = discord.Intents.default()
default_intents.members = True 
default_intents.message_content = True

bot = commands.Bot(command_prefix="!", intents=default_intents)

def in_channel(channel_name):
    def predicate(ctx):
        return ctx.channel.name == channel_name
    return commands.check(predicate)

@bot.command(name="register")
@in_channel("identite")
async def get_info(ctx):
    user_info = ctx.message.content.split(" ")
    if(len(user_info) != 7):
        await ctx.channel.send("usage : \n!register [prenom] [nom] [promotion] [filiere] [ville] [pays]")
        return

    prenom = user_info[1]
    nom = user_info[2]
    promotion = user_info[3]
    filiere = user_info[4].upper().replace("F", "")
    ville = user_info[5]
    pays = user_info[6]
    discord = f'{ctx.author}'.replace("#", "%23")

    user = requests.post(f'{api}user?discord={discord}&name={nom}&firstname={prenom}&promotion={promotion}&filiere={filiere}&city={ville}&country={pays}', headers=headers, verify=False)
    
    if user.status_code == 200:
        await ctx.message.add_reaction("✅")
        await ctx.channel.send(f"Merci {prenom} !")
    elif user.status_code == 409:
        await ctx.message.add_reaction("❌")
        await ctx.channel.send(f"Vous êtes déjà enregistré !")
    else:
        await ctx.message.add_reaction("❌")
        await ctx.channel.send(f"Une erreur est survenue...")
        
@bot.command(name="archive")
@in_channel("archive")
async def archive(ctx):
    attachments = ctx.message.attachments
    user_info = ctx.message.content.split(" ")
        
    if len(user_info) < 3 and ctx.message.content.count("/") != 1:
        await ctx.message.add_reaction("❌")
        await ctx.message.channel.send(content="usage : \n!archive [nom] / [description]]")
        return

    if len(attachments) == 0:
        await ctx.message.add_reaction("❌")
        await ctx.message.channel.send(content="Pas de fichier trouvé")
        return
    
    name = ctx.message.content.removeprefix("!archive").split("/")[0].strip()
    description = ctx.message.content.removeprefix("!archive").split("/")[1].strip()
    discord = f'{ctx.author}'.replace("#", "%23")
    



    payload = {'fullName' : name, 'description' : description}
    archive = requests.post(f'{api}archive?discord={discord}', json=payload, headers=headers)
    
    if archive.status_code == 200:
        await ctx.message.add_reaction("✅")
    elif archive.status_code == 409:
        await ctx.message.add_reaction("❌")
        await ctx.channel.send(f"Vous n'êtes pas enregistré.\nRendez-vous dans le channel {mention_channel(ctx.guild.channels,'identite')} !")
        return
    else:
        await ctx.message.add_reaction("❌")
        await ctx.channel.send(f"Une erreur est survenue... {archive}")
        return

    archive_id = archive.json().get("id")
    
    upload_success = True

    for attachment in attachments:
        image = requests.get(attachment.url)
        uploaded_image = requests.post(f'{api}archives/{archive_id}/ImagesUpload?name={name}&description={description}', files={'file': image.content})
        if uploaded_image.status_code != 200:
            upload_success = False

    if upload_success:
        await ctx.message.channel.send(content=f'Archive "{name}" mise en ligne avec {len(attachments)} document(s) !')
    else:
        await ctx.message.channel.send(content=f'Une erreur est survenue...')

@bot.event
async def on_member_join(member):
    await discord.utils.get(member.guild.channels, name='général').send(content=f'Salut {member.mention} !\nRendez-vous dans le channel {mention_channel(member.guild.channels, "identite")} pour t\'enregistrer !)')

def mention_channel(channels, channel_name):
    return discord.utils.get(channels, name=channel_name).mention

bot.run(token)