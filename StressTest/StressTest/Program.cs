﻿using NBomber.Contracts;
using NBomber.CSharp;
using NBomber.Plugins.Http.CSharp;

var factory = HttpClientFactory.Create();
var api = "http://virtualdrops.cf/api/";

IStep GenerateStep(string route)
{
    return Step.Create(route.Split('?').First(),
        clientFactory: factory,
        execute: context =>
        {
            var request = Http.CreateRequest("GET", $"{api}{route}");
            return Http.Send(request, context);
        });
}


var scenario = ScenarioBuilder
    .CreateScenario("api_stress",
        GenerateStep("Ping"),
        GenerateStep("Archive/1"),
        GenerateStep("Archives"),
        GenerateStep("Archives/Random"),
        GenerateStep("Archives/Search?search=test"),
        GenerateStep("Users")
    )
    .WithWarmUpDuration(TimeSpan.FromSeconds(15))
    .WithLoadSimulations(
        Simulation.KeepConstant(copies: 100, during: TimeSpan.FromSeconds(60))
    );

NBomberRunner
    .RegisterScenarios(scenario)
    .Run();