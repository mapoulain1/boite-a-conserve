#répartition par filière
SELECT count(*) as 'nombre d utilisateur', filiere.name as 'filière'FROM filiere
JOIN user ON user.filiere = filiere.id
GROUP BY filiere.name;

#moyenne image par archives
SELECT avg(nb_archives) FROM (
SELECT count(*) nb_archives FROM image
GROUP BY name
) as tmp;

#répartion par promo
SELECT count(*) as 'nombre d utilisateur', user.Promotion as 'promo'FROM user
GROUP BY user.Promotion;