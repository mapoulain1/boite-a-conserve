import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ArchiveService } from '../archive.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  value: string = '';
  screenToShow: string = '0';

  @Output()
  screen = new EventEmitter<string>();

  constructor(private _archiveService: ArchiveService) {}

  ngOnInit(): void {
  }

  public search() {
    this._archiveService.findArchive(this.value).subscribe(x => console.log(x));
    this.screen.emit(this.screenToShow);
    this.changeScreenToShow();
  }

  public changeScreenToShow() {
    if (this.screenToShow === '0')
      this.screenToShow = '1';
    else
      this.screenToShow = '0';
  }

}

