import { NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppCarouselModule } from './carousel/carousel.module';
import { AppSearchModule } from './search/search.module';
import { HttpClientModule } from '@angular/common/http';
import { AppSearchResultsModule } from './search-results/search-results.module';
import { CommonModule } from '@angular/common';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    AppCarouselModule,
    AppSearchModule,
    AppSearchResultsModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
