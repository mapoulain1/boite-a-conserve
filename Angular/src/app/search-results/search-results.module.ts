import { NgModule } from '@angular/core';
import { SearchResultsComponent } from './search-results.component';
import { CarouselModule } from 'primeng/carousel';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppCarouselModule } from './../carousel/carousel.module';

@NgModule({
  declarations: [
    SearchResultsComponent
  ],
  imports: [
    CarouselModule,
    BrowserAnimationsModule,
    AppCarouselModule
  ],
  exports: [
    SearchResultsComponent
  ],
  providers: [],
  bootstrap: [AppSearchResultsModule]
})
export class AppSearchResultsModule { }
