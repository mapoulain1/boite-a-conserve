import { Component, EventEmitter, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { Archive, ArchiveService } from '../archive.service';

@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.scss']
})
export class SearchResultsComponent {

  responsiveOptions: any;
  _archive?: Observable<Archive[]>;

  constructor(private _archiveService: ArchiveService) {
    this.responsiveOptions = [
        {
            breakpoint: '1024px',
            numVisible: 3,
            numScroll: 3
        },
        {
            breakpoint: '768px',
            numVisible: 2,
            numScroll: 2
        },
        {
            breakpoint: '560px',
            numVisible: 1,
            numScroll: 1
        }
    ];

    this._archive = this._archiveService.archive;
  }

}
