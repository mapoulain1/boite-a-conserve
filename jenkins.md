# BoiteDeConserve

```
pipeline {
    agent any
    stages {
        stage('Backend Unit Tests') { 
            steps {
                build job: "BoiteDeConserve.Backend.UnitTests"    
            }
        }
        stage('Backend') { 
            steps {
                build job: "BoiteDeConserve.Backend"    
            }
        }
        stage('Frontend') { 
            steps {
                build job: "BoiteDeConserve.Frontend"    
            }
        }
    }
}
```

# BoiteDeConserve.Frontend

```bash
cd $(mktemp -d)
git clone https://gitlab.isima.fr/mapoulain1/boite-a-conserve.git
docker build -t boite-de-conserve-frontend:$BUILD_NUMBER ./boite-a-conserve/Angular/
docker stop boite-de-conserve-frontend || true
docker rm boite-de-conserve-frontend || true
docker run --name boite-de-conserve-frontend -p 9003:80 -l "traefik.http.routers.boite-de-conserve-frontend.rule=PathPrefix(\`/\`)" -d boite-de-conserve-frontend:$BUILD_NUMBER
rm -rf boite-a-conserve
```

# BoiteDeConserve.Backend

```bash
cd $(mktemp -d)
git clone https://gitlab.isima.fr/mapoulain1/boite-a-conserve.git
cd boite-a-conserve/Backend
docker build -t boite-de-conserve-backend:$BUILD_NUMBER -f ./API/Dockerfile .
docker stop boite-de-conserve-backend || true
docker rm boite-de-conserve-backend || true
docker run --name boite-de-conserve-backend -p 9002:80 -l "traefik.http.routers.boite-de-conserve-backend.rule=PathPrefix(\`/api\`)" -d boite-de-conserve-backend:$BUILD_NUMBER
cd ../..
rm -rf boite-a-conserve
```

# BoiteDeConserve.UnitTests

```bash
cd $(mktemp -d)
git clone https://gitlab.isima.fr/mapoulain1/boite-a-conserve.git
cd boite-a-conserve/Database
docker build -t boite-de-conserve-db-tu:$BUILD_NUMBER -f DockerfileTU .
docker stop boite-de-conserve-db-tu || true
docker rm boite-de-conserve-db-tu || true
docker run -d -p 9101:3306 --name boite-de-conserve-db-tu -d boite-de-conserve-db-tu:$BUILD_NUMBER
sleep 30 #Waiting for Mysql to wake up
docker exec boite-de-conserve-db-tu sh -c 'mysql -u dev --password=dev boitedeconserve < DropEnv.sql'
docker exec boite-de-conserve-db-tu sh -c 'mysql -u dev --password=dev boitedeconserve < InitEnv.sql'
docker exec boite-de-conserve-db-tu sh -c 'mysql -u dev --password=dev boitedeconserve < InitEnvUnitTest.sql'
cd ../..

cd boite-a-conserve/Backend
docker build -t boite-de-conserve-backend-tu:$BUILD_NUMBER -f ./API/DockerfileTU .
docker stop boite-de-conserve-backend-tu || true
docker rm boite-de-conserve-backend-tu || true
docker run --name boite-de-conserve-backend-tu boite-de-conserve-backend-tu:$BUILD_NUMBER

docker stop boite-de-conserve-db-tu || true
docker stop boite-de-conserve-backend-tu || true
docker rm boite-de-conserve-db-tu || true
docker rm boite-de-conserve-backend-tu || true

cd ../..
rm -rf boite-a-conserve
```

# BoiteDeConserve.Database
```bash
cd $(mktemp -d)
git clone https://gitlab.isima.fr/mapoulain1/boite-a-conserve.git
cd boite-a-conserve/Database
docker build -t boite-de-conserve-db:$BUILD_NUMBER .
docker stop boite-de-conserve-db || true
docker rm boite-de-conserve-db || true
docker run -d -p 9001:3306 --name boite-de-conserve-db -d boite-de-conserve-db:$BUILD_NUMBER
sleep 30 #Waiting for Mysql to wake up
docker exec boite-de-conserve-db sh -c 'mysql -u dev --password=dev boitedeconserve < DropEnv.sql'
docker exec boite-de-conserve-db sh -c 'mysql -u dev --password=dev boitedeconserve < InitEnv.sql'
cd ../..
rm -rf boite-a-conserve
```

# BoiteDeConserve.Database.Dump

```bash
cd /home/max-server-1/Documents/database
mysqldump --host 82.66.59.41 --port 40001 -u dev --password="dev" --no-tablespaces boitedeconserve > dump_$(date '+%Y-%m-%d_%H-%M-%S').sql
```

# BoiteDeConserve.DiscordBot

```bash
cd $(mktemp -d)
git clone https://gitlab.isima.fr/mapoulain1/boite-a-conserve.git
cd boite-a-conserve/BotPython
docker build -t boite-de-conserve-discordbot:$BUILD_NUMBER .
docker stop boite-de-conserve-discordbot || true
docker rm boite-de-conserve-discordbot || true
docker run --name boite-de-conserve-discordbot -d boite-de-conserve-discordbot:$BUILD_NUMBER
cd ../..
rm -rf boite-a-conserve
```

# BoiteDeConserve.ImgPush

```bash
docker stop imgpush || true
docker rm imgpush || true
docker run -d --name imgpush -v /home/max-server-1/Documents/imgpush:/images -p 9005:5000 hauxir/imgpush:latest
```

# BoiteDeConserve.ImgPush.Dump

```bash
cd /home/max-server-1/Documents/
zip -r imgpush_dump/dump_$(date '+%Y-%m-%d_%H-%M-%S').zip imgpush/*
```

# BoiteDeConserve.ElasticKibana
```bash
cd $(mktemp -d)
git clone https://gitlab.isima.fr/mapoulain1/boite-a-conserve.git
cd boite-a-conserve/KibanaElastic
docker compose up -d
cd ../..
rm -rf boite-a-conserve
```

# BoiteDeConserve.ElasticKibana
```bash
cd $(mktemp -d)
git clone https://gitlab.isima.fr/mapoulain1/boite-a-conserve.git
cd boite-a-conserve/PrometheusGrafana
cp prometheus.yml /home/max-server-1/Documents/prometheus/prometheus.yml
docker compose up -d
cd ../..
rm -rf boite-a-conserve
```
