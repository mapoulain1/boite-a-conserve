# Boite deconserve

## Équipe

- Coralie PIERRE
- Maxime POULAIN
- Lucie SOANEN
- Aline TENAILLE


## Bien commencer

### Docker


* Portainer

```bat
docker run -d -p 9000:9000 -l "traefik.http.routers.portainer.rule=PathPrefix('/portainer')" --name portainer --restart always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce:2.15.1
```

* Jenkins

```bat
docker run -d -p 9004:8080 -p 50000:50000 -v jenkins-data:/var/jenkins_home -v /var/run/docker.sock:/var/run/docker.sock --name jenkins jenkins/jenkins:lts
```

* MySQL

Se placer dans le dossier Database.

```bat
docker build -t boite-de-conserve-db:a0.1 .
docker run -d -p 9001:3306 --name boite-de-conserve-db -d boite-de-conserve-db:a0.1
```

* Backend

Se placer dans le dossier Backend/boite-de-conserve.

```bat
docker build -t boite-de-conserve-backend:a0.1 -f ./API/Dockerfile .
docker run --name boite-de-conserve-backend -p 9002:80 -d boite-de-conserve-backend:a0.1
```

* Frontend

Se placer dans le dossier Frontend/boite-de-conserve.

```bat
docker build -t boite-de-conserve-frontend:a0.1 .
docker run --name boite-de-conserve-frontend -p 9003:80 -d boite-de-conserve-frontend:a0.1
```

* Reverse Proxy

Se placer dans le dossier ReverseProxy

```bat
docker build -t traefik:a0.1 .
docker run --name traefik -p 80:80 -p 443:443 -p 8080:8080 -v /var/run/docker.sock:/var/run/docker.sock -d traefik:a0.1
```


* Metabase

```bat
TODO
```

* Imgpush
```bat
docker run -d --name imgpush -v /home/max-server-1/Documents/imgpush:/images -p 9005:5000 hauxir/imgpush:latest
```

### Scaffolding de la bdd

```bat
scaffold-dbcontext "Server=82.66.59.41;Port=40001;Database=boitedeconserve;Uid=dev;Pwd=dev;" MySql.EntityFrameworkCore -f -d -Context Context -OutputDir Scaffold -Namespace BoiteDeConserve.Entities
```

## Architecture

<img src="https://i.ibb.co/mvSKg12/image-2022-10-12-164522070.png" width="800px">
