﻿using BoiteDeConserve.BLL.Interfaces;
using BoiteDeConserve.DTOs;
using Microsoft.AspNetCore.Mvc;

namespace BoiteDeConserve.API.Controllers
{
    [ApiController]
    public class ImageController : BaseController
    {
        private IImageManager ImageManager { get; }

        public ImageController(IImageManager imageManager)
        {
            ImageManager = imageManager;
        }

        [HttpGet]
        [Route("Archives/[controller]s/{filename}")]
        [ProducesResponseType(statusCode: 200, type: typeof(Stream))]
        public async Task<IActionResult> GetImage(string filename)
        {
            try
            {
                var image = await ImageManager.GetImage(filename);
                return Ok(image);
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        [HttpPost]
        [Route("Archives/{id}/[controller]s")]
        [ProducesResponseType(statusCode: 200, type: typeof(ImageDTO))]
        public async Task<IActionResult> AddImage(int id, [FromBody] ImageDTO imageDto)
        {
            try
            {
                imageDto.ArchiveId = id;
                var image = await ImageManager.AddImage(imageDto);
                return Ok(image);
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        [HttpPost]
        [Route("Archives/{id}/[controller]sUpload")]
        [ProducesResponseType(statusCode: 200, type: typeof(ImageDTO))]
        public async Task<IActionResult> UploadImage(int id, [FromForm] IFormFile file, string name, string description)
        {
            try
            {
                var imageDto = new ImageDTO
                {
                    ArchiveId = id,
                    Name = name,
                    Description = description,
                    Link = await ImageManager.UploadImage(file)
                };
                var image = await ImageManager.AddImage(imageDto);
                image.Link = ImageManager.GetImageUrl(imageDto.Link);
                return Ok(image);
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

    }
}
