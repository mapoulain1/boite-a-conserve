﻿using BoiteDeConserve.BLL.Interfaces;
using BoiteDeConserve.DTOs;
using Microsoft.AspNetCore.Mvc;

namespace BoiteDeConserve.API.Controllers
{

    [ApiController]
    public class UserController : BaseController
    {
        private IUserManager UserManager { get; }

        public UserController(IUserManager userManager)
        {
            UserManager = userManager;
        }

        [HttpGet]
        [Route("[controller]s")]
        [ProducesResponseType(statusCode: 200, type: typeof(ICollection<UserDTO>))]
        public async Task<IActionResult> Get(int page = 1, int limit = 100)
        {
            try
            {
                var users = await UserManager.GetUsers(page, limit);
                return Ok(users);
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        [HttpGet]
        [Route("[controller]/{discordId}")]
        [ProducesResponseType(statusCode: 200, type: typeof(UserDTO))]
        public async Task<IActionResult> Get(string discordId)
        {
            try
            {
                var user = await UserManager.GetUser(discordId);
                return Ok(user);
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        [HttpPost]
        [Route("[controller]")]
        [ProducesResponseType(statusCode: 200, type: typeof(UserDTO))]
        public async Task<IActionResult> Add(string discord, string name, string firstname, int promotion, int filiere, string city, string country)
        {
            try
            {
                if (await UserManager.Exist(discord))
                    return Conflict($"User {discord} already registered");
                
                var userDto = new UserDTO
                {
                    Discord = discord,
                    Name = name,
                    FirstName = firstname,
                    Promotion = promotion,
                    Filiere = new FiliereDTO { Name = $"F{filiere}"},
                    CityOrigin = city,
                    Country = country
                };
                var user = await UserManager.AddUser(userDto);
                return Ok(user);
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }
    }
}
