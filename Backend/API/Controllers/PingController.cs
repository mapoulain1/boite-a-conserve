﻿using Microsoft.AspNetCore.Mvc;

namespace BoiteDeConserve.API.Controllers
{
    [ApiController]
    public class PingController : BaseController
    {
        [HttpGet]
        [Route("[controller]")]
        public IActionResult Ping()
        {
            return Ok($"{DateTime.Now}");
        }
    }
}
