﻿namespace BoiteDeConserve.DTOs
{
    public class ArchiveDTO : IBaseDTO
    {
        public int Id { get; set; }
        public string? FullName { get; set; }
        public string? Description { get; set; }
        public DateTime? Date { get; set; }
        public int? OwnerId { get; set; }
        public ICollection<ImageDTO>? Images { get; set; }
    }
}
