﻿using AutoMapper;
using BoiteDeConserve.Entities;

namespace BoiteDeConserve.DTOs.Profiles
{
    public class FiliereMap : Profile
    {
        public FiliereMap()
        {
            CreateMap<Filiere, FiliereDTO>();
            CreateMap<FiliereDTO, Filiere>();
        }
    }
}
