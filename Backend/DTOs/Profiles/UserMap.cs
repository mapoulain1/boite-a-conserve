﻿using AutoMapper;
using BoiteDeConserve.Entities;

namespace BoiteDeConserve.DTOs.Profiles
{
    public class UserMap : Profile
    {
        public UserMap()
        {
            CreateMap<User, UserDTO>()
                .ForMember(dest => dest.Filiere, opt => opt.MapFrom(src => src.FiliereNavigation))
                .ForMember(dest => dest.Archives, opt => opt.Ignore());
            CreateMap<UserDTO, User>()
                .ForMember(dest => dest.FiliereNavigation, opt => opt.MapFrom(src => src.Filiere))
                .ForMember(dest => dest.Archives, opt => opt.Ignore())
                .ForMember(dest => dest.Filiere, opt => opt.Ignore());
        }
    }
}
