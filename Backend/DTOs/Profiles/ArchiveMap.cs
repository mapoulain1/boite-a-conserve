﻿using AutoMapper;
using BoiteDeConserve.Entities;

namespace BoiteDeConserve.DTOs.Profiles
{
    public class ArchiveMap : Profile
    {
        public ArchiveMap()
        {
            CreateMap<Archive, ArchiveDTO>();
            CreateMap<ArchiveDTO, Archive>();
        }
    }
}
