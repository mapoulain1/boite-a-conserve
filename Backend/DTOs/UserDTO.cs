﻿namespace BoiteDeConserve.DTOs
{
    public class UserDTO : IBaseDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string FirstName { get; set; }
        public int? Promotion { get; set; }
        public string CityOrigin { get; set; }
        public string Country { get; set; }
        public string Discord { get; set; }
        public FiliereDTO Filiere { get; set; }
        public ICollection<ArchiveDTO> Archives { get; set; }

    }
}
