﻿namespace BoiteDeConserve.DTOs
{
    public class FiliereDTO : IBaseDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
