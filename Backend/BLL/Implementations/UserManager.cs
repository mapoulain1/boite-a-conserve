﻿using AutoMapper;
using BoiteDeConserve.BLL.Interfaces;
using BoiteDeConserve.DTOs;
using BoiteDeConserve.Entities;
using Microsoft.EntityFrameworkCore;

namespace BoiteDeConserve.BLL.Implementations
{
    public class UserManager : BaseManager, IUserManager
    {
        public UserManager(IContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<ICollection<UserDTO>> GetUsers(int page, int limit)
        {
            if (page < 1) page = 1;
            if (limit is < 0 or > 100) limit = 10;
            var users = await Context.Users.Skip((page - 1) * limit).Take(limit).ToListAsync();
            return Mapper.Map<IList<UserDTO>>(users);
        }

        public async Task<UserDTO> GetUser(string discordId)
        {
            var user = await Context.Users.FirstAsync(x => x.Discord == discordId);
            return Mapper.Map<UserDTO>(user);
        }

        public async Task<bool> Exist(string discordId)
        {
            return await Context.Users.AnyAsync(x => x.Discord == discordId);
        }

        public async Task<UserDTO> AddUser(UserDTO userDto)
        {
            var user = Mapper.Map<User>(userDto);
            var filiere = await Context.Filieres.FirstAsync(x => x.Name == user.FiliereNavigation.Name);

            user.FiliereNavigation = filiere;
            user.Filiere = filiere.Id;

            var added = await Context.Users.AddAsync(user);
            await Context.SaveChangesAsync();
            return Mapper.Map<UserDTO>(added.Entity);
        }
    }
}
