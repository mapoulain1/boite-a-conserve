﻿using Newtonsoft.Json;

namespace BoiteDeConserve.BLL.Utils
{
    internal class ImgPushResponse
    {
        [JsonProperty("filename")]
        internal string Filename { get; set; }
    }
}
