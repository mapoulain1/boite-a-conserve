﻿using BoiteDeConserve.DTOs;

namespace BoiteDeConserve.BLL.Interfaces
{
    public interface IUserManager
    {
        public Task<ICollection<UserDTO>> GetUsers(int page, int limit);
        public Task<UserDTO> GetUser(string discordId);
        public Task<bool> Exist(string discordId);
        public Task<UserDTO> AddUser(UserDTO userDto);
    }
}
