﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

#nullable disable

namespace BoiteDeConserve.Entities
{
    [Table("user")]
    [Index(nameof(Filiere), Name = "filiere")]
    public partial class User
    {
        public User()
        {
            Archives = new HashSet<Archive>();
            Filieres = new HashSet<Filiere>();
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("name")]
        [StringLength(256)]
        public string Name { get; set; }
        [Column("first_name")]
        [StringLength(256)]
        public string FirstName { get; set; }
        [Column("promotion")]
        public int? Promotion { get; set; }
        [Column("filiere")]
        public int? Filiere { get; set; }
        [Column("city_origin")]
        [StringLength(256)]
        public string CityOrigin { get; set; }
        [Column("country")]
        [StringLength(256)]
        public string Country { get; set; }
        [Column("discord")]
        [StringLength(256)]
        public string Discord { get; set; }

        [ForeignKey(nameof(Filiere))]
        [InverseProperty("Users")]
        public virtual Filiere FiliereNavigation { get; set; }
        [InverseProperty(nameof(Archive.Owner))]
        public virtual ICollection<Archive> Archives { get; set; }
        [InverseProperty("Responsable")]
        public virtual ICollection<Filiere> Filieres { get; set; }
    }
}
