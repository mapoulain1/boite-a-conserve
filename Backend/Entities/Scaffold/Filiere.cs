﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

#nullable disable

namespace BoiteDeConserve.Entities
{
    [Table("filiere")]
    [Index(nameof(ResponsableId), Name = "filiere_responsable_id_fk")]
    public partial class Filiere
    {
        public Filiere()
        {
            Users = new HashSet<User>();
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("name")]
        [StringLength(256)]
        public string Name { get; set; }
        [Column("responsable_id")]
        public int? ResponsableId { get; set; }

        [ForeignKey(nameof(ResponsableId))]
        [InverseProperty(nameof(User.Filieres))]
        public virtual User Responsable { get; set; }
        [InverseProperty(nameof(User.FiliereNavigation))]
        public virtual ICollection<User> Users { get; set; }
    }
}
