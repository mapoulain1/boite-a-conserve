﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

#nullable disable

namespace BoiteDeConserve.Entities
{
    [Table("archive")]
    [Index(nameof(OwnerId), Name = "owner_id")]
    public partial class Archive
    {
        public Archive()
        {
            Images = new HashSet<Image>();
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("full_name")]
        [StringLength(256)]
        public string FullName { get; set; }
        [Column("description")]
        [StringLength(256)]
        public string Description { get; set; }
        [Column("date", TypeName = "date")]
        public DateTime? Date { get; set; }
        [Column("owner_id")]
        public int? OwnerId { get; set; }

        [ForeignKey(nameof(OwnerId))]
        [InverseProperty(nameof(User.Archives))]
        public virtual User Owner { get; set; }
        [InverseProperty(nameof(Image.Archive))]
        public virtual ICollection<Image> Images { get; set; }
    }
}
