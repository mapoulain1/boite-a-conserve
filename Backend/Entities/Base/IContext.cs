﻿using Microsoft.EntityFrameworkCore;

namespace BoiteDeConserve.Entities
{
    public interface IContext
    {
        public DbSet<Archive> Archives { get; set; }
        public DbSet<Filiere> Filieres { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Image> Images { get; set; }
        int SaveChanges();
        Task<int> SaveChangesAsync();
    }
}
